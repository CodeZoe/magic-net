﻿using Furion.DependencyInjection;
using Magic.Core;
using Magic.FlowCenter.Entity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.FlowCenter.Service;

	public interface IFlcFormManageService: ITransient
{
    Task Add(AddFlcFormInput input);
    Task Delete(PrimaryKeyParam input);
    Task Update(EditFlcFormInput input);
    Task<FlcForm> Get(PrimaryKeyParam input);
    Task<List<FlcForm>> List(QueryFlcFormPageInput input);
    Task<PageList<FlcForm>> PageList(QueryFlcFormPageInput input);
}
