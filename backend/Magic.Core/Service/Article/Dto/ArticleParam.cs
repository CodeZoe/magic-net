﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Magic.Core.Service;

#region 输入参数
/// <summary>
/// 分页查询文章参数
/// </summary>
public class QuerySysArticleInput : PageParamBase
{
    /// <summary>
    /// 标题
    /// </summary>
    public string Title { get; set; }

    /// <summary>
    /// 类型（字典 1通知 2公告）
    /// </summary>
    public ArticleType Type { get; set; }
}

/// <summary>
/// 新增文章参数
/// </summary>
public class AddSysArticleInput
{
    /// <summary>
    /// 标题
    /// </summary>
    [Required(ErrorMessage = "Title不能为空")]
    public string Title { get; set; }

    /// <summary>
    /// 内容
    /// </summary>
    [Required(ErrorMessage = "Content不能为空")]
    public string Content { get; set; }

    /// <summary>
    /// 类型（字典 1通知 2公告）
    /// </summary>
    public ArticleType Type { get; set; }
}

/// <summary>
/// 新增文章参数
/// </summary>
public class EditSysArticleInput : AddSysArticleInput
{
    /// <summary>
    /// 文章id
    /// </summary>
    public long Id { get; set; }
} 
#endregion

#region 输出参数
/// <summary>
/// 分页返回参数
/// </summary>
public class SysArticlePageOutput
{
    public long Id { get; set; }
    /// <summary>
    /// 标题
    /// </summary>
    public string Title { get; set; }

    /// <summary>
    /// 类型（字典 1通知 2公告）
    /// </summary>
    public int Type { get; set; }
    /// <summary>
    /// 文章类型描述
    /// </summary>
    public string TypeDesc
    {
        get { return Enum.GetName(typeof(ArticleType), this.Type); }
    }
}

/// <summary>
/// 详情返回参数
/// </summary>
public class SysArticleDetailOutput
{
    /// <summary>
    /// 标题
    /// </summary>
    public string Title { get; set; }

    /// <summary>
    /// 内容
    /// </summary>
    public string Content { get; set; }

    /// <summary>
    /// 类型（字典 1通知 2公告）
    /// </summary>
    public int Type { get; set; }

    /// <summary>
    /// 更新时间
    /// </summary>
    public DateTime CreatedTime { get; set; }

    /// <summary>
    /// 创建者
    /// </summary>
    public string CreatedUserName { get; set; }
}

#endregion