﻿
using Furion.DependencyInjection;
using Magic.Core.Entity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Magic.Core.Service;

public interface IDocumentService: ITransient
{
    Task<long> Add(AddDocumentInput input);
    Task Delete(PrimaryKeyParam input);
    Task<Documentation> Get(PrimaryKeyParam input);
    Task<PageList<QueryDocumentPageOutput>> Page(QueryDocumentPageInput input);
    Task Update(EditDocumentInput input);

    Task<List<DocumentTreeOutPut>> Tree();

    Task Upload(DocumentUploadInput input);
    Task UploadFolder(DocumentUploadInput input);
    Task Deletes(BatchDeleteDocumentInput input);
    Task Move(MoveDocumentInput input);
    Task<IActionResult> Download(PrimaryKeyParam input);
    Task<string> Preview(Core.PrimaryKeyParam input);
}
