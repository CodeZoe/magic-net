﻿using Furion.DynamicApiController;
using Magic.Core.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Magic.Web.Core;

/// <summary>
/// 服务器信息服务
/// </summary>
[AllowAnonymous]
[ApiDescriptionSettings(Name = "Machine", Order = 100, Tag = "服务器信息服务")]
public class SysMachineController : IDynamicApiController
{
    private readonly IMachineService _service;
    public SysMachineController(IMachineService service)
    {
        _service = service;
    }

    /// <summary>
    /// 获取服务器资源信息
    /// </summary>
    /// <returns></returns>
    [HttpGet("/sysMachine/use")]
    public async Task<dynamic> GetMachineUseInfo()
    {
        return await _service.GetMachineUseInfo();
    }

    /// <summary>
    /// 获取服务器基本参数
    /// </summary>
    /// <returns></returns>
    [HttpGet("/sysMachine/base")]
    public async Task<dynamic> GetMachineBaseInfo()
    {
        return await _service.GetMachineBaseInfo();
    }

    /// <summary>
    /// 动态获取网络信息
    /// </summary>
    /// <returns></returns>
    [HttpGet("/sysMachine/network")]
    public async Task<dynamic> GetMachineNetWorkInfo()
    {
        return await _service.GetMachineNetWorkInfo();
    }
}
